
#include <Servo.h> // 서보모터 헤더를 가져옴
#include <SoftwareSerial.h>
#include <SPI.h>
#include <MFRC522.h>


/* Define the DIO used for the SDA (SS) and RST (reset) pins. */
#define SDA_DIO 10
#define RESET_DIO 9

MFRC522 mfrc522(SDA_DIO, RESET_DIO);

Servo myservo1;  // create servo object to control a servo // 서보모터 객체를 생성함.

Servo myservo2;  // create servo object to control a servo // 서보모터 객체를 생성함.


int pinButton = 7; //사료주는 버튼 핀 번호를 2번으로 설정

int pinServo1 = 3; // 서보모터의 핀을 3번으로 설정

int pinServo2 = 5; // 서보모터의 핀을 5번으로 설정 

int aAngle = 0; // 현재 각도를 30으로 설정
int bAngle = 65; // 현재 각도를 170으로 설정

int cAngle = 30; // 현재 각도를 0으로 설정(초기값
int tAngle = 190; // 목표 각도를 180도로 설정

void resetAngle1() { // 서보모터의 각도를 리셋함
  myservo1.write(cAngle); // 0도로
}

void resetAngle2() { // 서보모터의 각도를 리셋함
  myservo2.write(aAngle); // 0도로
}

void setup() { // 초기 환경설정
 
  SPI.begin();
  mfrc522.PCD_Init();

  Serial.begin(9600); // 개발을 위해서 통신채널을 9600비트레이트로 맞춤 -> 시리얼 모니터로 확인 가능
  myservo1.attach(pinServo1);  // 지정해둔 서보모터의 핀 3번과 나의 서보모터를 연결
  myservo2.attach(pinServo2);  // 지정해둔 서보모터의 핀 5번과 나의 서보모터를 연결

  pinMode(pinButton, INPUT); // 먹이 버튼은 입력 전용

  pinMode(pinServo1, OUTPUT); // 서보모터는 표현(출력) 전용
  pinMode(pinServo2, OUTPUT); // 서보모터는 표현(출력) 전용

  resetAngle1(); // 각도 초기화.
  resetAngle2(); // 각도 초기화.
}

void check_rfid(){
  Serial.println("check_rfid");
  if(mfrc522.PICC_IsNewCardPresent()){
    Serial.println("PICC_IsNewCardPresent");
    if(mfrc522.PICC_ReadCardSerial()){
      Serial.print("Tag ID : ");
      
      String tagID="";
      
      for(int i = 0; i<=(mfrc522.uid.size); i++)
      {
          tagID += String(mfrc522.uid.uidByte[i]<0x10?"0":"");
          tagID += String(mfrc522.uid.uidByte[i],HEX);
      }

      Serial.println(tagID);
      mfrc522.PICC_HaltA();
      dropCocoa();
      dropWater(); // 먹이를 주는 액션 함수 실행
    }
  }  
}

void loop() { // 무한 반복
    check_rfid();
    delay(100);
}

void dropWater() { // 먹이를 주는 액션 함수

  for ( int i = cAngle ; i < tAngle ; i++) { // 서보모터를 움직여서 열게 함
    myservo1.write(i);
    delay(2); // 각도 1도당 0.002초의 지연시간을 넣어줘서 90도가 돌아가는 동안 약 0.2초의 안정적인 작동시간을 줌.
  }

  delay(19000);  // 사료가 11초동안 떨어지게 한다.

  for ( int i = tAngle ; i > cAngle ; i--) { // 서보모터를 움직여서 닫게 함
    myservo1.write(i);
    delay(2);
  }
}
void dropCocoa() {

  for ( int i = aAngle ; i < bAngle ; i++) { // 서보모터를 움직여서 열게 함
    myservo2.write(i);
    delay(2); // 각도 1도당 0.002초의 지연시간을 넣어줘서 90도가 돌아가는 동안 약 0.2초의 안정적인 작동시간을 줌.
  }

  delay(3000);  // 사료가 11초동안 떨어지게 한다.

  for ( int i = bAngle ; i > aAngle ; i--) { // 서보모터를 움직여서 닫게 함
    myservo2.write(i);
    delay(2);
  }
}
