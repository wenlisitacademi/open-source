#include <SoftwareSerial.h>

//v1.01 long time test succeed


String SSID="Wenly's iPhone8";
String PASSWORD="1234554321";

String serial = "KDJ001";
//String SSID="Wenly Home";
//String PASSWORD="1234512345";

String hostURL = "yachawm.cafe24.com";

//AT+CWMODE=1
//AT+CIPMUX=1

bool cwmode = false;
bool cipmux = false;
bool wifi_conn = false;
bool server_conn = false;
bool sent_data = false;
bool receive_data = false;

bool command = false;

String ccmd = "ready";

int adjustLength = 0;


const int pin_bright = A0;
const int pin_rain = A1;


void setCwmode(){
  Serial.println("setCwmode");
  ccmd = "setCwmode";  
  Serial1.println("AT+CWMODE=1");
  
}

void setCipmux(){
  Serial.println("setCipmux");
  ccmd = "setCipmux";  
  Serial1.println("AT+CIPMUX=1");
  
}

void connectWifi(){
  
  disconnectWifi();
  delay(2000);
  
  Serial.println("connectWifi");
  ccmd = "connectWifi";
  String cmd ="AT+CWJAP=\""+SSID+"\",\""+PASSWORD+"\"";
  Serial1.println(cmd);

  delay(1000);

  if(Serial1.find("OK")){
    //wifi_conn=true;
    Serial.println("Wifi connected");

  }else{
    Serial.println("Cannot connect to Wifi");
    wifi_conn=false;
  }

}

void disconnectWifi(){
  Serial.println("disconnectWifi");
  ccmd = "disconnectWifi";  
  Serial1.println("AT+CWQAP");
}




bool isStringInclude(String fullString, String searchString){
  if(fullString.indexOf(searchString) != -1)return true;
  return false;
}

void writeString(String stringData) { // Used to serially push out a String with Serial.write()

  for (int i = 0; i < stringData.length(); i++)
  {
    Serial.write(stringData[i]);   // Push each char 1 by 1 on each loop pass
  }

}

void connectServer(){
  Serial.println("connectServer");
  ccmd = "connectServer";
  String cmd_connect = "AT+CIPSTART=4,\"TCP\",\""+hostURL+"\",80";
  
  Serial1.println(cmd_connect);
  
}

void sendRequest(String uri){
  sent_data = true;
  ccmd = "sendRequest";
  
  String cmd_http = "GET ";
  cmd_http+=uri;
  cmd_http+=" HTTP/1.1";
  cmd_http+="\r\n";
  cmd_http+="Host: ";
  cmd_http+=hostURL;
  cmd_http+="\r\n";
  cmd_http+="Connection: keep-alive";
  cmd_http+="\r\n";
  cmd_http+="Accept-Charset: ISO-8859-1,UTF-8;q=0.7,*;q=0.7\r\n";
  cmd_http+="Accept-Language: de,en;q=0.7,en-us;q=0.3\r\n";
  cmd_http+="Cache-Control: no-cache\r\n";

  cmd_http+="\r\n";
  
  String cmd_cpi = "AT+CIPSEND=4,";
  cmd_cpi+= (cmd_http.length()-adjustLength);
  //adjustLength++;

  Serial.println(cmd_cpi);
  Serial1.println(cmd_cpi);
  
  delay(100);

  Serial.println(cmd_http);
  Serial1.println(cmd_http);
  
}






void setup() {

  // put your setup code here, to run once:

  Serial1.begin(115200);

  Serial.begin(115200);

}

void loop() {
  //return;
  if(command==false){
    // put your main code here, to run repeatedly:
    if(ccmd=="ready"){
      Serial.println("run ccmd");
      Serial.println(ccmd);
      if(cwmode==false)setCwmode();
      
      else if(cipmux==false)setCipmux();
      
      else if(wifi_conn==false)connectWifi();
    
      else if(server_conn==false)connectServer();
      //if(wifi_conn&&!server_conn)httpGet(hostURL,"/game/sensor_data.php?token=+|11|12|13|14|+");
    
      else if(sent_data==false){

        String rs = "/game/sensor_data.php?token=+|"+serial+"|";
        rs+= 1024-analogRead(pin_rain);
        rs+= "|";
        rs+= 1024-analogRead(pin_bright);
        rs+= "|+";
        
        sendRequest(rs);
      }
      else{
        Serial.println("command error");
        sent_data = false;
      }
  
    }
    
    if (Serial1.available()) {       
  
      String wmsg = Serial1.readString();
      writeString(wmsg);
  
      if( isStringInclude(wmsg, "OK") ){
        //Serial.println("OK Recognized");
        if(ccmd=="setCwmode"){
          cwmode = true;
        }
        else if(ccmd=="setCipmux"){
          cipmux = true;
        }
        else if(ccmd=="connectWifi"){
          wifi_conn = true;
          //Serial.println("Wifi 연결완료");
        }
        else if(ccmd=="connectServer"){
          server_conn = true;
        }
        else if(ccmd=="sendRequest"){
          sent_data = false;
          receive_data = true;
        }
        ccmd="ready";
      }
      else if(isStringInclude(wmsg, "not valid") ){
        server_conn = false;
        connectServer();
      }
      if(isStringInclude(wmsg, "busy") ){
        delay(200);
      }
      else if( isStringInclude(wmsg, "ERROR") || isStringInclude(wmsg, "FAIL") || isStringInclude(wmsg, "CLOSED") ){
        delay(200);
        
        if(ccmd=="setCwmode"){
          cwmode = false;
          cipmux=false;
          server_conn = false;
          wifi_conn = false;
          sent_data = false;
          setCwmode();
        }
        else if(ccmd=="setCipmux"){
          cwmode = true;
          connectWifi();
        }
        else if(ccmd=="connectWifi"){
          cipmux=false;
          server_conn = false;
          wifi_conn = false;
          sent_data = false;
          setCipmux();
        }
        else if(ccmd=="connectServer"){
          server_conn = false;
          wifi_conn = false;
          sent_data = false;
          connectWifi();
        }
        else if(ccmd=="sendRequest"){
          server_conn = false;
          sent_data = false;
          connectServer();
        }

      }
      
    }

  }

  while (Serial.available()) { 
    command = true;
    Serial1.write(Serial.read());   
  }

}
