// 1411 Left Slave Nano Old bootloader 328P
#include <SoftwareSerial.h>
#include <SPI.h>
#include <MFRC522.h>

/*

PINOUT:

RC522 MODULE    Uno/Nano     MEGA
SDA             D10          D9
SCK             D13          D52
MOSI            D11          D51
MISO            D12          D50
IRQ             N/A          N/A
GND             GND          GND
RST             D9           D8
3.3V            3.3V         3.3V

*/

/* Define the DIO used for the SDA (SS) and RST (reset) pins. */
#define SDA_DIO 10
#define RESET_DIO 9
/* Create an instance of the RFID library */

SoftwareSerial BTSerial(2, 3);   

MFRC522 mfrc522(SDA_DIO, RESET_DIO);



void check_rfid(){
  Serial.println("check_rfid");
  if(mfrc522.PICC_IsNewCardPresent()){
    Serial.println("PICC_IsNewCardPresent");
    if(mfrc522.PICC_ReadCardSerial()){
      Serial.print("Tag ID : ");
      
      String tagID="";
      
      for(int i = 0; i<=(mfrc522.uid.size); i++)
      {
          tagID += String(mfrc522.uid.uidByte[i]<0x10?"0":"");
          tagID += String(mfrc522.uid.uidByte[i],HEX);
      }

      Serial.println(tagID);

      
      writeStringToBT(tagID);
      
      mfrc522.PICC_HaltA();

      //delay(500);

      

    }
  }  
  
}

void setup() {  

  SPI.begin();
  mfrc522.PCD_Init();
  
  Serial.begin(9600);
  BTSerial.begin(9600);

  
  Serial.println("KSW_S v1.00"); 
}

void writeStringToBT(String stringData) { // Used to serially push out a String with Serial.write()

  for (int i = 0; i < stringData.length(); i++)
  {
    BTSerial.write(stringData[i]);   // Push each char 1 by 1 on each loop pass
    delay(15);
  }

}

void writeString(String stringData) { // Used to serially push out a String with Serial.write()

  for (int i = 0; i < stringData.length(); i++)
  {
    Serial.write(stringData[i]);   // Push each char 1 by 1 on each loop pass
  }

}


void loop() {

  BTSerial.listen();
  
  while(Serial.available()){
    BTSerial.write(Serial.read());  
  }
  
  while(BTSerial.available()){
    char ch = BTSerial.read();
    Serial.write(ch);  
    //BTSerial.write(ch);
  }

  check_rfid();

  delay(100);

}
