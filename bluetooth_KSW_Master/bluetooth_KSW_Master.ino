// 1421 Right Master
#include <SoftwareSerial.h>
#include <LiquidCrystal.h>

SoftwareSerial BTSerial(8, 9);   
LiquidCrystal lcd(12, 11, 2, 3, 4, 5);

int credit = 10000;
                                                       
void setup() {  
  Serial.begin(9600);
  BTSerial.begin(9600);
  lcd.begin(16,2);
}

void loop() { 

  BTSerial.listen();
  
  while(Serial.available()){
    BTSerial.write(Serial.read());  
  }
  
  while(BTSerial.available()){
    char ch = BTSerial.read();
    Serial.write(ch);  
    BTSerial.write(ch);
    credit-=100;
  }

  //String credit = "10000";
  lcd.clear();
  lcd.print("Credit:");
  lcd.print(credit);

  delay(100);

}
