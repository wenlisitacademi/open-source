const int sensor_pin = D0;

void setup()
{
  Serial.begin(9600);
}

void loop()
{
 float moisture_percentage;

 moisture_percentage = analogRead(sensor_pin);

 //Serial.print("Soil Moisture(in Percentage)=");
 Serial.println(moisture_percentage);
 //Serial.println("%");

 delay(10);
}
