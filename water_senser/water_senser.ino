int analogPin = A0;
int led = 13;


void setup() {
  
  pinMode(led, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  int val = analogRead(analogPin);
  Serial.println(val);
  if(val>100){
    digitalWrite(led,HIGH);
  }
  else{
    digitalWrite(led,LOW);
  }
  delay(20);
  
}
