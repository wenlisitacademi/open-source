/*
    This sketch establishes a TCP connection to a "quote of the day" service.
    It sends a "hello" message, and then prints received data.
*/

#include <ESP8266WiFi.h>
#include<Wire.h>

const int MPU=0x68;//MPU6050 I2C주소

int AcX,AcY,AcZ;//,Tmp,GyX,GyY,GyZ;
float ax, ay, az;
float prevForce = 0;
float forces = 0;

void get6050();

#ifndef STASSID
#define STASSID "Wenly's iPhone8"
#define STAPSK  "1234554321"
#endif

const char* ssid     = STASSID;
const char* password = STAPSK;

const char* host = "yachawm.cafe24.com";
const uint16_t port = 80;

int tick = 0;

void setup() {

  Wire.begin();

  Wire.beginTransmission(MPU);

  Wire.write(0x6B);

  Wire.write(0);//MPU6050 을 동작 대기 모드로 변경

  Wire.endTransmission(true);

  
  Serial.begin(57600);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void get6050(){

  Wire.beginTransmission(MPU);//MPU6050 호출

  Wire.write(0x3B);//AcX 레지스터 위치 요청

  Wire.endTransmission(false);

  Wire.requestFrom(MPU,6,true);//14byte의 데이터를 요청

  AcX=Wire.read()<<8|Wire.read();//두개의 나뉘어진 바이트를 하나로 이어붙입니다.
  ax = AcX;
  AcY=Wire.read()<<8|Wire.read();
  ay = AcY;
  AcZ=Wire.read()<<8|Wire.read();
  az = AcZ;
//  Tmp=Wire.read()<<8|Wire.read();
//
//  GyX=Wire.read()<<8|Wire.read();
//
//  GyY=Wire.read()<<8|Wire.read();
//
//  GyZ=Wire.read()<<8|Wire.read();

}

bool isStringInclude(String fullString, String searchString){
  if(fullString.indexOf(searchString) != -1)return true;
  return false;
}

void loop() {
  tick++;
  
  get6050();//센서값 갱신
  float force = sqrt(ax*ax+ay*ay+az*az);

  
  if(prevForce!=0){
    float fa = abs(force-prevForce);
    prevForce = force;
    forces+=fa;  
    Serial.println(fa);
  }
  else{
    prevForce = force;
  }
  

  if(tick%10==0){

    Serial.print("connecting to ");
    Serial.print(host);
    Serial.print(':');
    Serial.println(port);
    
    // Use WiFiClient class to create TCP connections
    WiFiClient client;
    if (!client.connect(host, port)) {
      Serial.println("connection failed");
      delay(5000);
      return;
    }
  
    // This will send a string to the server
    Serial.println("sending data to server");
    if (client.connected()) {
      String rq = "GET /game/acc_data.php?token=+|ACC001|";
      rq+=forces;
      rq+="|+";
      
      client.print(rq + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n" +
               "\r\n"
              );
    }
  
    // wait for data to be available
    unsigned long timeout = millis();
    while (client.available() == 0) {
      if (millis() - timeout > 5000) {
        Serial.println(">>> Client Timeout !");
        client.stop();
        delay(60000);
        return;
      }
    }
  
    // Read all the lines of the reply from server and print them to Serial
    Serial.println("receiving from remote server");
    // not testing 'client.connected()' since we do not need to send data here

    String responseString = "";
    
    while (client.available()) {
      char ch = static_cast<char>(client.read());
      Serial.print(ch);
      responseString += ch;
    }

    if(isStringInclude(responseString, "success")){
      Serial.println("reset forces");
      forces = 0;
    }
  
    // Close the connection
    Serial.println();
    Serial.println("closing connection");
    client.stop();
  }
  

  delay(10); // execute once every 5 minutes, don't flood remote service
}
