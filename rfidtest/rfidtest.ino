#include <SoftwareSerial.h>
#include <SPI.h>
#include <MFRC522.h>


/*

PINOUT:

RC522 MODULE    Uno/Nano     MEGA
SDA             D10          D9
SCK             D13          D52
MOSI            D11          D51
MISO            D12          D50
IRQ             N/A          N/A
GND             GND          GND
RST             D9           D8
3.3V            3.3V         3.3V

*

* Include the standard Arduino SPI library */
//#include 
/* Include the RFID library */
//#include 

/* Define the DIO used for the SDA (SS) and RST (reset) pins. */
#define SDA_DIO 10
#define RESET_DIO 9

MFRC522 mfrc522(SDA_DIO, RESET_DIO);

void setup() {

  pinMode(49,INPUT);
  pinMode(53,INPUT);

  Serial.begin(9600);
 
  SPI.begin();
  mfrc522.PCD_Init();
  Serial.println("RFID & BLUETOOTH TEST! v1.00"); 
}

void check_rfid(){
  Serial.println("check_rfid");
  if(mfrc522.PICC_IsNewCardPresent()){
    Serial.println("PICC_IsNewCardPresent");
    if(mfrc522.PICC_ReadCardSerial()){
      Serial.print("Tag ID : ");
      
      String tagID="";
      
      for(int i = 0; i<=(mfrc522.uid.size); i++)
      {
          tagID += String(mfrc522.uid.uidByte[i]<0x10?"0":"");
          tagID += String(mfrc522.uid.uidByte[i],HEX);
      }

      Serial.println(tagID);
      mfrc522.PICC_HaltA();

    }
  }  
}

void loop(){

  check_rfid();

  delay(100);
  
}
